Users of HEWES are asked to cite the following papers in publications

```bibtex
@article{Lindneretal2023,
title = {Numerical simulations of the nonlinear quantum vacuum in the Heisenberg-Euler weak-field expansion},
journal = {Journal of Computational Physics: X},
volume = {17},
pages = {100124},
year = {2023},
issn = {2590-0552},
doi = {10.1016/j.jcpx.2023.100124},
url = {https://www.sciencedirect.com/science/article/pii/S2590055223000021},
author = {Andreas Lindner and Baris Ölmez and Hartmut Ruhl},
keywords = {Vacuum polarization, Heisenberg-Euler, Weak-field expansion, Simulation, High harmonics, Birefringence},
abstract = {The nonlinear Heisenberg-Euler theory is capable of describing the dynamics of vacuum polarization, a key prediction by quantum electrodynamics. Due to vast progress in the field of laser technology in recent years vacuum polarization can be triggered in the lab by colliding high-intensity laser pulses, leading to a variety of interesting novel phenomena. Since analytical methods for highly nonlinear problems are generally limited and since the experimental requirements for the detection of the signals from the nonlinear quantum vacuum are high, the need for numerical support is apparent. The paper presents a highly-accurate, efficient numerical scheme for solving the nonlinear Heisenberg-Euler equations in weak-field expansion up to six-photon interactions. Properties of the numerical scheme are discussed and an implementation accurate up to order thirteen in terms of spatial resolution is given. Simulations are presented and benchmarked with known analytical results. The versatility of the numerical solver is demonstrated by solving problems in complicated configurations.}
}
```

```bibtex
@article{Lindneretal2023a,
title = {HEWES: Heisenberg–Euler weak-field expansion simulator},
journal = {Software Impacts},
volume = {15},
pages = {100481},
year = {2023},
issn = {2665-9638},
doi = {https://doi.org/10.1016/j.simpa.2023.100481},
url = {https://www.sciencedirect.com/science/article/pii/S2665963823000180},
author = {Andreas Lindner and Baris Ölmez and Hartmut Ruhl},
keywords = {Quantum vacuum, Heisenberg–Euler, Simulations, Photon–photon interactions},
abstract = {Vacuum polarization, a key prediction of quantum theory, can cause a variety of intriguing phenomena that can be triggered by high-intensity laser pulses. The Heisenberg–Euler theory of the quantum vacuum supplements Maxwell’s theory of electromagnetism with nonlinear photon–photon interactions mediated by vacuum fluctuations. This work presents a numerical solver for the leading weak-field Heisenberg–Euler corrections. The present code implementation reaches an accuracy of order thirteen in the numerical scheme and takes into account up to six-photon interactions. Since theoretical approaches are limited to approximations and the experimental requirements for signal detection are high, the need for numerical support is apparent.}
}
```
